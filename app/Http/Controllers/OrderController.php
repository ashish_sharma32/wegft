<?php

namespace App\Http\Controllers;

use App\Order;
use App\Cart;
use App\Mail\OrderConfirm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use Response;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(Auth::check()){
        $customer = $request->all();;
        $order = new Order;
        $validator = $order->putSession($customer);
        if($validator){
            return redirect()->back()->withErrors($validator)->withInput(Input::all());
        }
        else{
          $oldCart = Session::get('cart');
          $cart = new Cart($oldCart);
          $newOrder = $order->newOrder($cart);
          if($newOrder == "order_created"){
            return view('views_app.shop.checkout_success')->with(array('products'=>$cart->items,'totalPrice'=>$cart->totalPrice));
          }
          else if($newOrder == "coupon_value_less"){
            return redirect()->back()->with('error_coupon','coupon value less');
          }
          else{
            return redirect()->back();
          }
        }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($order = Order::find($id)){
          if($order->user_id == Auth::user()->id){
            $cart = unserialize($order->cart);
            return view('views_app.shop.order-details')->with(array('products'  => $cart->items ,'order' => $order));
          }
          else{
            return Response::json('Forbidden', 403);
          }
        }
        else{
           return Response::json('Record Not Found', 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

}
