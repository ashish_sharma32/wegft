<?php

namespace App\Http\Controllers;
use App\Product;
use App\Cart;
use Session;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        if($product = Product::where('slug',$slug)->first()){
            return view('views_app.shop.single-product')->with('product',$product);    
        }
        else{
            abort(404);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function addToCart(Request $request, $slug) {
        if($request->ajax()){
            if($product = Product::where('slug', $slug)->first()){
                $oldCart = Session::has('cart') ? Session::get('cart') : null;
                $cart = new Cart($oldCart);
                $cart->add($product,$product->id);
                Session::put('cart', $cart);
                return "success";    
            }
        }
        else{
            abort(403);
        }
    }
    
    public function removeFromCart(Request $request, $slug) {
        if($product = Product::where('slug', $slug)->first()){
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->remove($cart, $product->id);
            Session::put('cart', $cart);
            if (Session::get('cart')->totalQty<1) {
                Session::forget('cart');
            }
            return redirect()->back();
        }
        else{
            abort(403);
        }
    }

    public function getCart(Request $request) {
        if (!Session::has('cart')) {
            return response()->json("Empty Cart");
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);        
        return response()->json(['cart'=>$cart->items,'totalQty' =>$cart->totalQty,'totalPrice' => $cart->totalPrice]);
    }

    public function renderProductsView($products) {
        if(!$products->isEmpty())
        {
        $render = ' ';
        foreach($products as $product) {
            $pslug = $product->slug;
            $pimg = $product->image;
            $pname = $product->name;
            $pprice = $product->price;
            $pcategory = $product->category->name;
            $pdesc = $product->description;
            $render.= "<div class='product-item-element col-sm-6 col-md-4 col-lg-3 wow fadeIn' data-wow-delay='0.1s'><div class='product-item'><div class='product-item-inner'><div class='product-img-wrap'> <img src='$pimg'/></div><div class='product-button'> <a href='' class='add_cart' data-slug='$pslug' ><i class='add_icon fa fa-shopping-bag' ></i></a> <a href='/products/$pslug' ><i class='fa fa-search'></i></a></div></div><div class='product-detail'><p class='product-title'><a href='/'>$pname</a></p> <strong class='item-price'>$pprice - weGFT Credits</strong></div></div></div>";
            $id =$product->id;
        }
        return response()->json([
            'products' => $render,
            'id' => $id
        ]);    
        }
        else{
            return $products;
        }        
    }

    public function loadProducts(Request $request){
        $products = new Product;
        $products = $products->loadProducts($request);
        return $this->renderProductsView($products);
    }

    public function search($q){
        $products = Product::whereHas('category', function($query) use($q) {
            $query->where('name', 'like', '%'.$q.'%');})
                ->orWhere('name', 'LIKE', '%'.$q.'%')->limit('4')->get();
        return view('views_app.shop.search')->with(array('products'=>$products,'q'=>$q));      
    }
}