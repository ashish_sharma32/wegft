<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Session;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $validator = $user->storeUser();
        
        if($validator)
        {
            return $validator->messages();
        }
        else
        {   
            return "success";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function login(Request $request){
        $user = new User;
        $validator = $user->loginUser();
        if($validator)
        {
            return redirect()->back()->withErrors($validator);
        }
        else
        {
            $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
            ];

            if(Auth::attempt($credentials))
            {
                if(Auth::user()->admin == true)
                {
                    return redirect('/admin');
                }
                return redirect('/')->with('message', 'login');   
            }
            else{
                return redirect()->back()->with('logintry', 'wronglogin');   
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/');             
    }
}
