<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Order;
use App\User;
use Session;
use Auth;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class CouponController extends Controller {

    private $woo_consumer_secret;
    private $woo_consumer_key;

    public function __construct() {
        $this->woo_consumer_secret = env('WOO_CONSUMER_SECRET');
        $this->woo_consumer_key = env('WOO_CONSUMER_KEY');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon) {

    }
    
    public function search($code) {
        $client = new Client(['base_uri' => 'https://www.dankendo.com/wp-json/wc/v2/']);
        try {
        $items = json_decode($client->request('GET', 'coupons', ['query' => ['consumer_key' =>
        $this->woo_consumer_key, 'consumer_secret' => $this->woo_consumer_secret]])->getBody());
        $coupon = null;
        foreach($items as $item) {
            if ($item->code == $code && $item->email_restrictions['0']  == Auth::user()->email) {
                $coupon = $item;
                break;
            }
        }
        } catch (RequestException $e) {
            return null;
        }

        return $coupon;
    }

    public function redeem(Request $request){
        $result = $this->search($request->code);
        if($result!=null){
            if(Order::where('coupon_code', $request->code)->exists()){
            return "used";
            }
            $coupon = new Coupon;
            $coupon->add($result->id,$request->code,$result->meta_data['0']->value,$result->amount);
            Session::put('coupon', $coupon);
            $this->getBillingFromCoupon();
            return "success";
        }
        else{
            return "error";
        }
    }

    public function redeemFromRefer($code){
      $result = $this->search($code);
      if(Session::has('coupon') && Session::get('coupon')->code == $code){
        $result = 'has';
      }
      else{
        if($result!=null){
          if(Order::where('coupon_code', $code)->exists()){
            $result = 'used';
          }
          else{
          $coupon = new Coupon;
          $coupon->add($result->id,$code,$result->meta_data['0']->value,$result->amount);
          Session::put('coupon', $coupon);
          $this->getBillingFromCoupon();
          }
        } 
      }

      return view('views_app.redeem_result')->with('result',$result);
    }

    public function getBillingFromCoupon(){
        $client = new Client(['base_uri' => 'https://www.dankendo.com/wp-json/wc/v2/']);
        $order_id = Session::get('coupon')->orderId;
        $billing = json_decode($client->request('GET', "orders/$order_id", ['query' => ['consumer_key' =>
        $this->woo_consumer_key, 'consumer_secret' => $this->woo_consumer_secret]])->getBody());
        $billing = $billing->billing;
        $user = User::find(Auth::id());
        $user->address_street = $billing->address_1 . " " . $billing->address_2;
        $user->address_city = $billing->city;
        $user->address_state = $billing->state;
        $user->address_zip = $billing->postcode;
        $user->phone = $billing->phone;
        $user->save();
    }

    public function hasCoupon(){
        if(Session::has('coupon')){
            $coupon_code = Session::get('coupon')->code;
            $coupon_amount = Session::get('coupon')->amount;
            $coupon = array('code' => $coupon_code , 'amount' => $coupon_amount);
            return $coupon;
        }
        else{
            return "empty";
        }
    }

    public function removeCoupon(){
        $hasCoupon = Session::has('coupon') ? true : null;
        $hasCoupon ? Session::forget('coupon') : null;
        if($hasCoupon){
         return "success";   
        }
    }
}