<?php

namespace App\Http\Controllers;
use App\Product;
use App\Categories;
use App\Order;
use Session;


use Illuminate\Http\Request;

class AdminController extends Controller
{
  public function login(){
  	return view('views_admin.login');
  }
              
  public function dashboard(){
  	return view('views_admin.dashboard');
  }

  // Products //
  
  public function products(){
  	$products = Product::all();
  	return view('views_admin.products')->with('products',$products);
  }

  public function createProduct(){
    $categories = Categories::all()->pluck('name');
    return view('views_admin.products_create')->with('categories',$categories);
  }

  public function storeProduct(Request $request){
    $input = $request->all();
    $product = new Product;
    $validator = $product->store($request);
    if ($validator) {
        return redirect()->back()->withErrors($validator);
    } else {
        return redirect('/admin/products');
    }
  }

  public function editProduct($product){
    $product = Product::where('slug',$product)->first();
    $categories = Categories::all()->pluck('name');
    return view('views_admin.products_edit')->with(['product'=>$product, 'categories' => $categories]);
  }

  public function updateProduct(Request $request, $id){
    $product = new Product;
    $validator = $product->updateProduct($id,$request);
    if ($validator) {
        return redirect()->back()->withErrors($validator)->withInput($request->all());
    } else {
        return redirect('/admin/products');
    }
  }
  
  public function deleteProduct($id){
    $product = Product::findOrFail($id);
    if ($product->delete()) {
      Session::forget('cart');
      return "success";
    }
    else{
      return "error";
    }
  }

  // Categories //

  public function categories(){
    $categories = Categories::all();
    return view('views_admin.categories')->with('categories',$categories);
  }

  public function createCategory(){
    return view('views_admin.categories_create');
  }

  public function storeCategory(Request $request){
    $input = $request->all();
    $category = new Categories;
    $validator = $category->store($request);
    if ($validator) {
      return redirect()->back()->withErrors($validator);
    } else {
      return redirect('/admin/categories');
    }
  }

  public function editCategory($category){
    $category = Categories::where('slug',$category)->first();
    return view('views_admin.categories_edit')->with('category',$category);
  }

  public function updateCategory(Request $request, $id){
    $category = new Categories;
    $validator = $category->updateCategory($request,$id);
    if ($validator) {
        return redirect()->back()->withErrors($validator)->withInput($request->all());
    } else {
        return redirect('/admin/categories');
    }
  }

  public function deleteCategory($id){
    $category = Categories::findOrFail($id);
    if(Product::where('category_id', '=', $id)->exists()){
      return "error";
    }
    else{
      $category->delete();
      Session::forget('cart');
      return "success";
    }
  }

  // Orders //

  public function orders(){
    $orders = Order::all();
    return view('views_admin.orders')->with('orders',$orders);
  }

  public function showOrder($order){
    if($order = Order::find($order)){
      $cart = unserialize($order->cart);
      return view('views_admin.orders_view')->with(array('products'  => $cart->items ,'order' => $order));  
    }
  }

  public function deleteOrder($id){
    $order = Order::findOrFail($id);
    if ($order->delete()){
      Session::forget('cart');
      return "success";
    }else{
      return "error";
    } 
  }
}