<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Exception;
use App\User;
use Socialite;

class SocialAuthController extends Controller
{
    public function redirectToProvider($provider){
        try {
            return Socialite::driver($provider)->redirect();    
        } catch (Exception $e) {
            return redirect('/login')->with('login_response','fail');
        }
    }
    
    public function handleProviderCallback($provider){
        try{
            $user = Socialite::driver($provider)->user();
            $authUser = $this->findOrCreateUser($user, $provider);
            Auth::login($authUser, true);
            return redirect('/')->with('login_response', 'ok');
        } catch(Exception $e){
            return redirect('/login')->with('login_response','fail');
        }
    }

    public function findOrCreateUser($user, $provider){
        $authUser = User::where('email', $user->email)->first();
        if ($authUser) {
            return $authUser;
        }
        else{
            $fullname = json_decode($this->split_name($user->name));
            return User::create([
            'firstname'     =>  $fullname->fname,
            'lastname'      =>  $fullname->lname,
            'email'         =>  $user->email,
            'provider'      =>  $provider,
            'provider_id'   =>  $user->id,
            'avatar'        =>  $user->avatar,
            ]);
        }
    }
    
    public function split_name($name){
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        $fullname = array('fname' => $first_name, 'lname' => $last_name);
        return json_encode($fullname);
    }
}
