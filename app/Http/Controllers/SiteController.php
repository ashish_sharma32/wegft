<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Product;
use App\Cart;
use App\Order;
use Auth;
use Session;

class SiteController extends Controller
{   
    public function home(){
        $products = Product::limit(8)->get();
        return view('views_app.home')->with('products',$products);    		
    }

    public function about(){
        return view('views_app.about');
    }

    public function shop(){
        $products = Product::paginate(8);
    	return view('views_app.shop.shop')->with('products',$products);		
    }

    public function account(){
        $orders = Order::where('user_id',Auth::user()->id)->get();
        return view('views_app.shop.my-account')->with('orders',$orders); 
    }
     
    public function singleProduct(){
        return view('views_app.shop.single-product');
    }

    public function contact(){  
    	return view('views_app.contact');
    }

    public function login(){
        return view('views_app.login');
    }

    public function register(){
        return view('views_app.register');
    }

    public function cart(){
        if (!Session::has('cart')) {
            $products = null;
            return view('views_app.shop.cart')->with('products', $products);
        }
        $cart = Session::get('cart');
        return view('views_app.shop.cart')->with(array('products'=>$cart->items,'totalPrice'=>$cart->totalPrice));
    }

    public function checkout(){
        if (!Session::has('cart')) {
            $products = null;
            return view('views_app.shop.cart')->with('products', $products);
        }
        $cart = Session::get('cart');
        return view('views_app.shop.checkout')->with(array('products'=>$cart->items,'totalPrice'=>$cart->totalPrice));  
    }
}