<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
  public $id;
  public $code;
  public $orderId;
  public $amount;
  
  public function add($id,$code,$orderId,$amount){
  	$this->id = $id;
  	$this->code = $code;
  	$this->orderId = $orderId;
  	$this->amount = $amount;
  } 
}
