<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Request;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable=[
        'firstname',
        'lastname',
        'email',
        'password',
        'provider',
        'provider_id',
        'admin',
        'avatar',
        'remember_token'];

    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    }

    public function rules()
    {
        return $rules = [
        'firstname'=> 'required|min:2|alpha',
        'lastname'=> 'required|min:2|alpha',
        'email' => 'required|email|unique:users',
        'password'=> 'required|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/|between:8,12'
        ];
    }

    public function loginRules()
    {
        return $rules = [
        'email'=> 'required|email',
        'password'=> 'required|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/|between:8,12'
        ];
    }
    
    public function storeUser(){
    	$user = new User;
        $validator = Validator::make(Request::all(), $this->rules());
        if($validator->passes())
        {
            User::create(Request::all());
        }
        else
        {
            return $validator;
        }
    }

    public function loginUser(){
    	$validator = Validator::make(Request::all(), $this->loginRules());
        if(!$validator->passes())
        {   
            return $validator;
        }
    }
}
