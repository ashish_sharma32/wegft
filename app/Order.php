<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;
use App\Mail\OrderConfirm;
use Validator;
use Session;
use Auth;
use Illuminate\Http\Request;

class Order extends Model
{
  protected $fillable=[
    'user_id',
    'cart',
    'totalQty',
    'totalAmount',
    'firstname',
    'lastname',
    'email',
    'phone',
    'address_street',
    'address_city',
    'address_zip',
    'address_state',
    'coupon_code',
    'orderstatus_id'
  ];

  public function rules() {
    return $rules=[
    'firstname'=>'required|min:3|alpha',
    'lastname'=>'required|min:3|alpha',
    'email'=>'required|email',
    'phone'=>'required|between:7,10',
    'address_street'=>'required|min:2',
    'address_city'=>'required|min:2',
    'address_zip'=>'required|min:2',
    'address_state'=>'required|min:2',
    'coupon_code'=>'required|min:2',
    ];
  }

  public function putSession($customer) {
    $validator=Validator::make($customer, $this->rules());
    if ($validator->passes()) {
      Session::put('customer', $customer);
    }
    else {
      return $validator;
    }
  }

  public function newOrder($cart){
    $order = new Order;
    try {
      if($cart->totalPrice>Session::get('coupon')->amount){
        return "coupon_value_less";
      }
      $order->cart=serialize($cart);
      $order->totalQty=$cart->totalQty;
      $order->totalAmount=$cart->totalPrice;
      $order->user_id=Auth::user()->id;
      $order->firstname=Session::get('customer')['firstname'];
      $order->phone=Session::get('customer')['phone'];
      $order->lastname=Session::get('customer')['lastname'];
      $order->email=Session::get('customer')['email'];
      $order->address_street=Session::get('customer')['address_street'];
      $order->address_city=Session::get('customer')['address_city'];
      $order->address_zip=Session::get('customer')['address_zip'];
      $order->address_state=Session::get('customer')['address_state'];
      $order->coupon_code=Session::get('coupon')->code;
      $order->orderstatus_id = 1;
      $order->save();
      $this->sendOrderConfirmMail($order->id);
      Session::forget('customer');
      Session::forget('cart');
      Session::forget('coupon');
      return "order_created";
    } catch (\Illuminate\Database\QueryException $e) {
      abort(403);
    }
  }

  public function sendOrderConfirmMail($orderId){
    $order = Order::findOrFail($orderId);
    Mail::to(Session::get('customer')['email'])->send(new OrderConfirm($order));
  }
}
