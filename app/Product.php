<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\Input;

class Product extends Model
{
  protected $fillable=['name','price','image'];

  public function category() {
  	return $this->belongsTo('App\Categories');
  }

  public function createRules(){
	  return $rules = [
	      'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u|unique:products',
	      'slug' => 'unique:products',
	      'category_id' => 'required',
	      'price' => 'required|numeric',
	      'image' => 'required'
	  ];
  }

  public function updateRules($id) {
      return $rules = [
          'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u|unique:products,name,'.$id,
          'slug' => 'unique:products,slug'.$id,
          'category_id' => 'required',
          'price' => 'required|numeric'
      ];
  }

  public function store($request){
  	$product = new Product;
	  $validator = Validator::make($request->all(), $this->createRules());
	  if ($validator->passes()) {
      /*Get Image, Move to Folder and Get Final Path*/
      $imgDestinationPath = 'uploads/products';
      $imgExtension = $request->file('image')->getClientOriginalExtension();
      $productImage = str_slug($request->name,'-').'.'.$imgExtension;
      $request->file('image')->move($imgDestinationPath, $productImage);
      $imgPath = "/uploads/products/".$productImage;
      $product->name = $request->name;
      $product->slug = str_slug($request->name,'-');
      $product->category_id = $request->category_id;
      $product->price = $request->price;
      $product->image = $imgPath;
      $product->save();
    } else {
      return $validator;
    }
  }

  public function updateProduct($id,$request) {
    $product = Product::find($id);
    $validator = Validator::make($request->all(), $this->updateRules($id));
    if ($validator -> passes()) {
      $image = Input::file('image');
      if ($image) {
        $imgDestinationPath = 'uploads/products';
        $imgExtension = $request->file('image')->getClientOriginalExtension();
        $productImage = str_slug($request->name,'-').'.'.$imgExtension;
        $request->file('image')->move($imgDestinationPath, $productImage);
        $imgPath = "/uploads/products/".$productImage;
      } else {
          $imgPath = $product->image;
      }
      $product->fill(array('name' => $request->get('name'),
          'slug' => str_slug($request->get('name'), '-'),
          'price' => $request->get('price'),
          'category_id' => $request->get('category_id'),
          'image' => $imgPath))->save();
    } else {
        return $validator;
    }
  }

  public function loadProducts($request){
    $products = Product::where('id','>',$request->lastProductId)->limit(8)->get();
    return $products;
  }
}

