<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Request;
use Validator;
use Auth;

class Categories extends Model
{
	protected $fillable=['name','slug'];
	
  public function products(){
  	return $this->hasMany('App\Product','category_id');
  }

  public function createRules(){
	  return $rules = [
      'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u|unique:categories',
      'slug' => 'unique:categories'
	  ];
  }

  public function updateRules($id)
  {
    return $rules = [
    'name' => 'required|min:3|regex:/^[(a-zA-Z0-9\s)]+$/u|unique:categories,name,'.$id,
    'slug' => 'unique:categories,slug,'.$id 
    ];
  }

  public function store($request){
    $category = new Categories;
    $validator = Validator::make($request->all(), $this->createRules());
	  if ($validator->passes()) {
      $category->create(array('name'=> $request->get('name'),
        'slug'=> str_slug($request->get('name'), '-')));
	  } else {
      return $validator;
    }
  }

  public function updateCategory($request,$id) {
    $validator = Validator::make($request->all(), $this->updateRules($id));
    if($validator->passes()){
      $category = Categories::find($id);
      $category->fill(array('name' => $request->get('name'),
      'slug' => str_slug($request->get('name'), '-')))->save();
    } else {
      return $validator;
    }
  }

  public function deleteCategory($id)
  {
    if (Categories::destroy($id)) {
      return true;
    }
    return false;
    
  }
}
