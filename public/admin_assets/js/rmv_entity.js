$(".rmv").click(function() {
  $form = $(this).parent();
  swal({
    title: "Are you sure?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
  if (willDelete) {
    $id = $form.data("id");
    $entity = $form.data("entity");
    $.ajax({
      type: "delete",
      url: "/admin/" + $entity + "/" + $id,
        data: {
          _token: $("input[name=_token]").val()
        },
        success: function(t) {
          if($entity == 'categories'){
            "error" == t ? swal("Cannot Delete", "Category Assosiated With Some Product(s)", "error") : location.reload(!0)  
          }
          else{
            "error" == t ? swal("Cannot Delete", "Some Errors Occurred ", "error") : location.reload(!0)          
          }
        }
      })
  } else {
  }
  });
});