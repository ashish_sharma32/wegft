$(function() {
  getCart();
  hasCoupon();    
    
    /*rev sLider*/

    var revapi;
    jQuery(document).ready(function() {   
      revapi = jQuery("#rev_slider").revolution({
        sliderType:"standard",
        sliderLayout:"fullwidth",
        navigation: {
          arrows:{enable:true}        
        },    
        autoHeight:"on",  
        gridwidth:1040,
        gridheight:400    
      });   
    }); /*ready*/

    /*Check If Coupon Exists*/
    function hasCoupon(){
      $.ajax({
        type: "get",
        url: "/has-coupon",
        success: function(e) {
          if(e=="empty"){
            $("#apply_code").show();
          }
          else{
            $("#apply_code").hide();
            $('.coupon-code').html(e.code);
            $(".coupon p").show();
            $(".wegft-credits-amount").html(e.amount + " weGFT Credits");
            $(".order-credits").show();
          }
        }
      });
    }

    /* Register User */
    myform = $("#user_register");
    myform.validate({
        rules: {
            firstname: {
                required: !0,
                minlength: 3
            },
            lastname: {
                required: !0,
                minlength: 3
            },
            password: {
                required: !0,
                minlength: 8,
                alphanumeric: !0
            }
        }
    });
    $("#user_register").submit(function(event) {
        event.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: "post",
            url: "/users",
            data: formData,
            processData: false,
            contentType: false,
            success: function(e) {
                if(e=="success"){
                  swal("Success", "User Registered Successfully", "success");
                }   
            }
        });
    });

    /* Get Cart */
    function getCart(){
    $('.cart-product-item').empty();
    $.ajax({
      type: "get",
      url: "/get-cart",
      success: function(e) {
          if(e=='Empty Cart')
          {
            $(".cart-empty").show();
            $(".cart-widget-footer").hide(); 
          }
          else{
            $(".cart-empty").hide();
            $(".cart-widget-footer").show();
            $(".cart-total-price").html(e.totalPrice + " weGFT Credits");
            $.each(e.cart, function(index, element) {
                $cartItem = "<li>";
                $cartItem += "<a href='#' class='product-image'><img src="+element.item.image+"></a>";
                $cartItem += "<div class='product-content'><a class='product-link' href='#'>"+element.item.name+"</a>";
                $cartItem += "<div class='cart-collateral'><span class='qty-cart'>"+element.qty+"</span>&nbsp;<span>&#215;</span>&nbsp;<span class='product-price-amount'>"+element.item.price+" weGFT Credits</span></div>";
                $cartItem += "<a class='product-remove' href='/remove-from-cart/"+element.item.slug+"'><i class='fa fa-times-circle' aria-hidden='true'></i></a></div>";
                $cartItem += "</li>";
              $('.cart-product-item').append($cartItem);
              $(".add_icon").attr('class', 'add_icon fa fa-shopping-bag');
            });
          }
        }
    });    
    }
    
    /* Add To Cart */
    $(document).on('click','.add_cart',function(event){
      event.preventDefault();
      $slug = $(this).data('slug');
      $(this).find( ".add_icon" ).attr('class', 'add_icon fa fa-spinner');
      $.ajax({
        type: "get",
        url: "/add-to-cart/"+$slug,
        success: function(e) {
          if(e=="success"){
            swal("Success", "Product Added To Cart", "success");
            getCart();
          }
        }
      });
    });

    /*Remove Code*/
    $('#remove_code').on('click', function(event) {
      event.preventDefault();
      $(".btn-apply").html("Apply Code");
      $.ajax({
        type: "get",
        url: "/remove-code",
        success: function(e) {
          if(e=="success"){
            $(".coupon p").hide();
            $(".order-credits").hide();
            $("#apply_code").show();
          }
        }
      });
    });

  /*Apply Code*/
  $('form#apply_code').submit(function(event) {
  event.preventDefault();
  $code = $('input[name=code]').val();
  $(".btn-apply").html("<i class='add_icon fa fa-spinner'></i>");
  var formData = new FormData(this);
  $.ajax({
    type: "post",
    data:formData,
    cache:false,
    contentType: false, 
    processData: false,
    url: "/redeem",
    success: function(e) {
      if(e=="success"){
        $.ajax({
        type: "get",
        url: "/has-coupon",
        success: function(e) {
            $(".wegft-credits-amount").html(e.amount + " weGFT Credits");
          }
        });
        $("#apply_code").hide();
        $('.coupon-code').html($code);
        $(".coupon p").show();
        $(".order-credits").show();
      }
      else{
        $(".btn-apply").html("Apply Coupon");
        if(e=="used"){
          swal("Oops!", "This Coupon Has Already Been Redeemed", "error");
        }
        if(e=="error"){
          swal("Oops!", "Coupon Not Found / Coupon Not Valid For Current User", "error");
        }
      }
    }
  });
  });

  /*Global Search*/
  $(document).on('submit', '#search', function(event) {
    event.preventDefault();
    var q = $("input[name='q']" ).val();
    if(q){
    window.location.replace("/search/"+q);  
    }
    else{
      swal("Empty Search Query", "Please Enter a Search Query", "error");
    }
  });
});