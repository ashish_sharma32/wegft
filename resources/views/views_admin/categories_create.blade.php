@extends('layout.admin')
@section('page-content')
<div class="row">
<div class="col-sm-12">
  <div class="panel panel-default panel-border-color panel-border-color-primary">
    <div class="panel-heading panel-heading-divider">
    Add New Category
    </div>
    <div class="panel-body">
      @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{ $error }}</div>
      @endforeach
      {{ Form::open(array('url' => '/admin/categories/store','files' => true)) }}
        <div class="form-group xs-pt-10">
          <label>Name</label>
          {!!Form::text('name',null, array('class' => 'form-control','required'))!!}
        </div>
        <div class="row xs-pt-15">
          <div class="col-xs-6">
            <p class="text-right">
              <button type="submit" class="btn btn-space btn-primary">Submit</button>
            </p>
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
</div>
@stop