@extends('layout.admin')
@section('page-content')
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">
          <div class="col-md-8 col-sm-12 col-xs-12">
              <p>Categories Management</p>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12">
              <a href="/admin/categories/create" class="btn btn-info">ADD NEW CATEGORY</a>
          </div>
      </div>
      <div class="panel-body">
        <table class="table">
        @if(count($categories)>0)
          <thead>
            <tr>
              <th style="width:50%;">Category Title</th>
              <th class="actions">Edit</th>
              <th class="actions">Delete</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($categories as $category)
            <tr>
              <td>{{$category->name}}</td>
              <td class="actions"><a class="icon" href="/admin/categories/{{$category->slug}}/edit"><i class="mdi mdi-edit"></i></a></td>
              <td class="actions">
              {!! Form::open(['method' => 'DELETE', 'url' => "/admin/categories/{{$category->id}}" , 'data-id' => $category->id, 'data-entity' => 'categories']) !!}
              {!! Form::button('Remove', ['class' => 'btn btn-danger rmv','data']) !!}
              {!! Form::close() !!}
              </td>
            </tr>
            @endforeach                  
          </tbody>
        @else
          <div class="col-md-12">
          <h3 style="text-align: center">No Category</h3>
          </div>
        @endif
        </table>
      </div>
    </div>
  </div>
</div>
@stop