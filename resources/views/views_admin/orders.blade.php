@extends('layout.admin')
@section('page-content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <p>Orders Management</p>
                </div>
            </div>
            <div class="panel-body">
              <table class="table">
              @if(count($orders)>0)
                <thead>
                    <tr>
                      <th class="actions">Order ID</th>
                      <th class="actions">Customer Name</th>
                      <th class="actions">Ship To</th>
                      <th class="actions">Order Date</th>
                      <th class="actions">Delete</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($orders as $order)
                  <tr>
                    <td class="actions"><a href="/admin/orders/{{$order->id}}">{{$order->id}}</a></td>
                    <td class="actions">{{$order->firstname}} {{$order->lastname}}</td>
                    <td class="actions">{{$order->address_street}} {{$order->address_city}}, {{$order->address_state}}, {{$order->address_zip}}</td>
                    <td class="actions">{{$order->created_at->toDateString()}}</td>
                    <td class="actions">
                      {!! Form::open(['method' => 'DELETE', 'url' => "/admin/orders/{{$order->id}}" , 'data-id' => $order->id, 'data-entity' => 'orders']) !!}
                      {!! Form::button('Remove', ['class' => 'btn btn-danger rmv','data']) !!}
                      {!! Form::close() !!}
                    </td>
                  </tr>
                  @endforeach                  
                </tbody>
              @else
                <div class="col-md-12">
                <h3 style="text-align: center">No Orders</h3>
                </div>
              @endif
              </table>
            </div>
        </div>
    </div>
</div>
@stop