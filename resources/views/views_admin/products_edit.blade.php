@extends('layout.admin')
@section('page-content')
<div class="row">
<div class="col-sm-12">
  <div class="panel panel-default panel-border-color panel-border-color-primary">
    <div class="panel-heading panel-heading-divider">
    Edit Product
    </div>
    <div class="panel-body">
      @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{ $error }}</div>
      @endforeach
      {{ Form::model($product,array('url'=> "/admin/products/$product->id" , 'method' => 'PUT', 'files' => true )) }}
        <div class="form-group xs-pt-10">
          <label>Name</label>
          {{ Form::text('name',null, array('class' => 'form-control')) }}
        </div>
        <div class="form-group xs-pt-10">
          <label>Category</label>
          {{ Form::select('category_id', $categories,null,['class' => 'table-group-action-input form-control input-medium', 'required']) }}
        </div>
        <div class="form-group xs-pt-10">
          <label>Price</label>
          {!!Form::number('price',null, array('class' => 'form-control'))!!}
        </div>
        <div class="form-group">
          <label>Image</label>
          {{ Form::file('image', ['class' => 'form-control']) }}
        </div>
        <div class="row xs-pt-15">
          <div class="col-xs-6">
            <p class="text-right">
              <button type="submit" class="btn btn-space btn-primary">Update</button>
            </p>
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
</div>
@stop