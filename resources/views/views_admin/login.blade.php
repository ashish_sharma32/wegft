<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials.admin.meta')
    @include('partials.admin.styles')
    @include('partials.admin.polyfills')
  </head>
  <body class="be-splash-screen">
    <div class="be-wrapper be-login">
      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="splash-container">
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">{{ $error }}</div>
              @endforeach
              @if(session()->has('logintry'))
                <div class="alert alert-danger">Wrong Username/Password Combination</div>
              @endif
            <div class="panel panel-default panel-border-color panel-border-color-primary">
              <div class="panel-heading"><img src="/admin_assets/img/logo-xx.png" alt="logo" width="102" height="27" class="logo-img"><span class="splash-description">Please enter admin login information.</span></div>
              <div class="panel-body">
                {{ Form::open(array('url' => '/login')) }}
                  <div class="form-group">
                    <input id="username" type="email" placeholder="Username" autocomplete="off" class="form-control" name="email" required="">
                  </div>
                  <div class="form-group">
                    <input id="password" type="password" placeholder="Password" class="form-control" name="password" required="">
                  </div>
                  <div class="form-group login-submit">
                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Sign me in</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @include('partials.admin.scripts')
  </body>
</html>