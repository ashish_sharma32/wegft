@extends('layout.app')
@section('title') Something Went Wrong - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="content-page">
    <div class="container mb-80">
      <div class="row">
        <div class="col-sm-12">
          <div class="text-center">
            <img src="/img/403.png" alt="Page Not Found" width="50%" class="img-responsive">
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@stop
