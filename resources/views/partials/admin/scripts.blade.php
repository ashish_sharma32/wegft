{!! Html::script('admin_assets/lib/jquery/jquery.min.js') !!}
{!! Html::script('admin_assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}
{!! Html::script('admin_assets/lib/bootstrap/dist/js/bootstrap.min.js') !!}
{!! Html::script('admin_assets/js/rmv_entity.js') !!}
{!! Html::script('https://unpkg.com/sweetalert/dist/sweetalert.min.js') !!}
{!! Html::script('admin_assets/js/main.js') !!}
<script type="text/javascript">
$(document).ready(function(){
  //initialize the javascript
  App.init();
});
</script>

