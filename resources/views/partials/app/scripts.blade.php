{!! Html::script('js/jquery.min.js') !!}
{!! Html::script('js/plugins/jquery-ui.js') !!}
{!! Html::script('js/plugins/modernizr.js') !!}
{!! Html::script('plugins/rev_slider/js/jquery.themepunch.revolution.min.js') !!}
{!! Html::script('plugins/rev_slider/js/jquery.themepunch.tools.min.js') !!}
{!! Html::script('plugins/rev_slider/js/revolution.extension.navigation.min.js') !!}
{!! Html::script('plugins/rev_slider/js/revolution.extension.parallax.min.js') !!}
{!! Html::script('plugins/rev_slider/js/revolution.extension.slideanims.min.js') !!}
{!! Html::script('plugins/rev_slider/js/revolution.extension.layeranimation.min.js') !!}
{!! Html::script('js/plugins/tether.min.js') !!}
{!! Html::script('js/plugins/bootstrap.min.js') !!}
{!! Html::script('js/plugins/owl.carousel.js') !!}
{!! Html::script('js/plugins/slick.js') !!}
{!! Html::script('js/plugins/jquery.validate.min.js') !!}
{!! Html::script('js/plugins/additional-methods.min.js') !!}
{!! Html::script('https://unpkg.com/sweetalert/dist/sweetalert.min.js') !!}
{!! Html::script('js/plugins/plugins-all.js') !!}
{!! Html::script('js/custom.js') !!}
{!! Html::script('js/main.js') !!}
@if(session()->get('login_response')=='ok')
<script type="text/javascript">
	swal("Success", "Logged In Successfully", "success");
</script>
@endif
@if(session()->get('login_response')=='fail')
<script type="text/javascript">
	swal("Error Logging In", "Please Try Again", "error");
</script>
@endif

