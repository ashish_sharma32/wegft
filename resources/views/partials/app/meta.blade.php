<meta charset="utf-8">
<title>@yield('title')</title>
<meta name="description" content="weGFT" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<link rel="shortcut icon" type="image/x-icon" href="/img/favicon.png">
<link rel="icon" type="img/png" href="/img/favicon.png">
<link rel="apple-touch-icon" href="/img/favicon.png">