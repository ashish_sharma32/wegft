@extends('layout.app')
@section('title') Login - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link"> <a href="#">Home</a> <span>Login & Register</span> </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="form-border-box">
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger">{{ $error }}</div>
            @endforeach
            @if(session()->has('logintry'))
            <div class="alert alert-danger">Wrong Username/Password Combination</div>
            @endif
            {!! Form::open() !!}
            <h2 class="normal"><span>Registered Customers</span></h2>
            <p>Please enter your registered login details to continue.</p>
            <div class="form-field-wrapper">
              <label>Enter Your Email <span class="required">*</span></label>
              <input class="input-md form-full-width" name="email" aria-required="true" required type="email">
            </div>
            <div class="form-field-wrapper">
              <label>Enter Your Password <span class="required">*</span></label>
              <input class="input-md form-full-width" name="password" aria-required="true" required type="password">
            </div>
            <div class="form-field-wrapper">
              <input name="submit" class="submit btn btn-md btn-color" type="submit">
            </div>
            {!! Form::close() !!}
            <hr/ class="hr-line-social">
            <p>Log In Using Social Networks</p>
            <div class="col-md-4 col-sm-4 social-inline">
              <a href="/auth/facebook"><img src="/img/social/fb.png" class="social-icons"></a>
            </div>
            <div class="col-md-4 col-sm-4 social-inline">
              <a href="/auth/twitter"><img src="/img/social/twitter.png" class="social-icons"></a>
            </div>
            <div class="col-md-4 col-sm-4 social-inline">
              <a href="/auth/google"><img src="/img/social/google.png" class="social-icons"></a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-border-box">
            <h2 class="normal"><span>New Customers</span></h2>
            <p>Not a Registered User, Click the button below to register.</p>
            <div class="form-field-wrapper">
              <a href="/register" class="submit btn btn-md btn-color">Create An Account</a>
            </div>
            <hr/ class="hr-line-social">
            <p>Register Using Social Networks</p>
            <div class="col-md-4 col-sm-4 social-inline">
              <a href="/auth/facebook"><img src="/img/social/fb.png" class="social-icons"></a>
            </div>
            <div class="col-md-4 col-sm-4 social-inline">
              <a href="/auth/twitter"><img src="/img/social/twitter.png" class="social-icons"></a>
            </div>
            <div class="col-md-4 col-sm-4 social-inline">
              <a href="/auth/google"><img src="/img/social/google.png" class="social-icons"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@stop