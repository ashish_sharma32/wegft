@extends('layout.app')
@section('title') Redeem Your Code - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link"> <a href="#">Home</a> <span>Redeem Your Coupon Code</span> </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="text-center">
          @if($result==null)
          <h3>Coupon Not Found / Not Valid For You</h3>
          <div><a class="btn btn-lg btn-color form-half-width" href="/shop">Back To Shop</a></div>
          @elseif($result=='has')
          <h3>Coupon Already Applied</h3>
          <div><a class="btn btn-lg btn-color form-half-width" href="/cart">Go To Cart</a></div>
          @elseif($result=='used')
          <h3>Coupon Already Used</h3>
          <div><a class="btn btn-lg btn-color form-half-width" href="/shop">Back To Shop</a></div>
          @else
            <h3>Coupon Applied Successfully</h3>
            <h5>Your Coupon Worth {{$result->amount}} weGFT Credits has been applied to cart</h5>
            <h5>Your Shipping Details Have Been Imported From Your Dan Kendo Order. To confirm, go to your account page.</h5>
            <div><a class="btn btn-lg btn-color form-half-width" href="/my-account">My Account</a></div>
          @endif
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@stop