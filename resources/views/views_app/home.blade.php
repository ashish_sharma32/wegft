@extends('layout.app')
@section('title') Home - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section id="intro" class="intro">
    <div id="rev_slider" class="rev_slider"  data-version="5.0">
      <ul>  
        <li data-transition="fade">
          <img src="img/slide-img/slide_bg1.jpg"  alt="">             
        </li>
      </ul>       
    </div>
  </section>
  <section id="promo" class="section-padding-sm promo ">
    <div class="container">
    </div>
  </section>
  <section class="section-padding-b">
    <div class="container">
      <h2 class="page-title">LATEST PRODUCTS</h2>
    </div>
    <div class="container">
      <div class="tab-content">
        <div id="latest" role="tabpanel" class="tab-pane fade in active">
          <div id="new-product" class="product-item-4 owl-carousel owl-theme nf-carousel-theme1">
            @foreach ($products as $product)
            <div class="product-item">
              <div class="product-item-inner">
                <div class="product-img-wrap"> <img src="{{$product->image}}" alt=""></div>
                <div class="product-button">
                  <a href="#" class="add_cart" data-slug="{{$product->slug}}"><i class="add_icon fa fa-shopping-bag" ></i></a> 
                  <a href="/products/{{$product->slug}}"><i class="fa fa-search"></i></a> 
                </div>
              </div>
              <div class="product-detail">
                <a class="tag" href="#">Men Fashion</a>
                <p class="product-title"><a href="/">{{$product->name}}</a></p>
                <div class="product-rating">
                  <div class="star-rating" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating" title="Rated 4 out of 5"> <span style="width: 60%"></span></div>
                  <a href="#" class="product-rating-count"><span class="count">3</span> Reviews</a>
                </div>
                <p class="product-description"> When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic remaining essentially unchanged.</p>
                <strong class="item-price">{{$product->price}} - weGFT Credits</strong>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="">
    <div class="section-padding container-fluid bg-image text-center overlay-light90 no-padding-top">
      <div class="container">
        <h2 class="page-title">Choose by Categories</h2>
      </div>
    </div>
    <div class="container container-margin-minus-t">
      <div class="row">
        <div class="col-md-6">
          <div class="categories-box">
            <div class="categories-image-wrap"> <img src="/img/categories/categories_tshirts.jpg" alt="" /></div>
            <div class="categories-content">
              <a href="#">
                <div class="categories-caption">
                  <h6 class="normal">T-Shirts</h6>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="categories-box">
            <div class="categories-image-wrap"> <img src="/img/categories/categories_vapes.jpg" alt="" /></div>
            <div class="categories-content">
              <a href="#">
                <div class="categories-caption">
                  <h6 class="normal">Vapes</h6>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="like-share" class="like-share section-padding">
  </section>
  <section class="">
    <div class="container container-margin-minus-t">
      <div class="home-about-blocks">
        <div class="col-12 about-blocks-wrap">
          <div class="row">
            <div class="col-sm-6 col-md-6 customer-say">
              <div class="about-box-inner">
                <h4 class="mb-25">Customer Say</h4>
                <div class="testimonials-carousel owl-carousel owl-theme nf-carousel-theme1">
                  <div class="product-item">
                    <p class="large quotes">Customer Testimonial</p>
                    <h6 class="quotes-people">- Customer Name</h6>
                  </div>
                  <div class="product-item">
                    <p class="large quotes">Customer Testimonial</p>
                    <h6 class="quotes-people">- Customer Name</h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6 about-shop">
              <div class="about-box-inner">
                <h4 class="mb-25">About weGFT</h4>
                <p class="mb-20">Welcome to <b class="black">weGFT</b> - Excerpt For weGFT About Us</p>
                <a href="/about" class="btn btn-xs btn-color">Learn More <i class="fa fa-angle-right right"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="brand-logo" class="section-padding brand-logo">
    <div class="container">
      <ul class="list-none-ib brand-logo-carousel owl-carousel owl-theme">
        <li class="brand-item">
          <a href="#"> <img src="img/logo/brand_clrgld.png" alt="clrgld" /> </a>
        </li>
        <li class="brand-item">
          <a href="#"> <img src="img/logo/brand_dankendo.png" alt="dankendo" /> </a>
        </li>
        <li class="brand-item">
          <a href="#"> <img src="img/logo/brand_nomnom.png" alt="nomnom" /> </a>
        </li>
      </ul>
    </div>
  </section>
</div>
@stop