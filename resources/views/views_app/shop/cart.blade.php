@extends('layout.app')
@section('title') Shopping Cart - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link">
            <a href="#">Home</a>
            <span>Cart</span>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page">
    <div class="container mb-80">
      <div class="row">
        <div class="col-sm-12">
          @if(is_null($products))
                <div class="text-center">
                  <h3>Shopping Cart Empty</h3>
                  <div><a class="btn btn-lg btn-color form-half-width" href="/">Back To Shop</a></div>
                </div>
          @else
          <article class="post-8">
            <div class="cart-product-table-wrap responsive-table">
              <table>
                <thead>
                  <tr>
                    <th class="product-remove"></th>
                    <th class="product-thumbnail"></th>
                    <th class="product-name">Product</th>
                    <th class="product-price">Price</th>
                    <th class="product-quantity">Quantity</th>
                    <th class="product-subtotal">Total</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($products as $product)
                  <tr>
                    <td class="product-remove">
                      <a href="/remove-from-cart/{{$product['item']->slug}}"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                    </td>
                    <td class="product-thumbnail">
                      <a>
                      <img src="{{$product['item']->image}}" alt=""></a>
                    </td>
                    <td class="product-name">
                      <a>{{$product['item']->name}}</a>
                    </td>
                    <td class="product-price">
                      <span class="product-price-amount amount">{{$product['item']->price}} weGFT Credits</span>
                    </td>
                    <td>
                      <div class="product-quantity">
                        <span class="product-qty">{{$product['qty']}}</span>
                      </div>
                    </td>
                    <td class="product-subtotal">
                      <span class="product-price-sub_totle amount">{{$product['qty'] * $product['item']->price }} weGFT Credits</span>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <div class="row cart-actions">
              <div class="col-md-6">
                <div class="coupon">
                  <p><a href='' id='remove_code'><i class='fa fa-times-circle' aria-hidden='true'></i></a> Coupon Code <strong class="coupon-code"></strong> Applied</p>
                  {{ Form::open(array('url' => '/redeem','id'=>'apply_code')) }}
                  <input class="input-md col-lg-8 col-md-8 col-sm-12 col-xs-12" name="code" title="Coupon Code" placeholder="Enter Coupon Code" type="text"><br/>
                  <button class="btn btn-md btn-black btn-apply col-lg-8 col-md-8 col-sm-12 col-xs-12" type="submit">Apply Coupon</button>
                  {{ Form::close() }}
                </div>
              </div>
            </div>
            <div class="cart-collateral">
              <div class="cart_totals">
                <h3>Cart totals</h3>
                <div class="responsive-table">
                  <table>
                    <tbody>
                      <tr class="cart-subtotal">
                      <tr class="order-total">
                        <th>Total</th>
                        <td><span class="product-price-amount amount" style="text-transform: none;">{{$totalPrice}} weGFT Credits</span>
                        </td>
                      </tr>
                      <tr class="order-credits">
                        <th> - (weGFT Credits Applied)</th>
                        <td><span class="product-price-amount amount wegft-credits-amount" style="text-transform: none;"></span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="product-proceed-to-checkout">
                  <a class="btn btn-lg btn-color form-full-width" href="/checkout">Proceed to checkout</a>
                </div>
              </div>
            </div>
          </article>
          @endif
        </div>
      </div>
    </div>
  </section>
</div>
@stop
