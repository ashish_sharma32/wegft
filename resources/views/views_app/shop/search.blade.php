@extends('layout.app')
@section('title') Search Results For {{$q}} - weGFT @stop
@section('page-content')
<div class="wraper">
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link"> <a href="/">Home</a> <span>Search</span> </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="list-page-title">
            <h2 class="">Search Results For {{$q}}</h2>
          </div>
          @if(count($products)>0)
          <div class="row product-list-item">
            @foreach ($products as $product)
            <div class="product-item-element col-sm-6 col-md-4 col-lg-3 wow fadeIn" data-wow-delay="0.1s">
              <div class="product-item">
              <div class="product-item-inner">
                <div class="product-img-wrap"> <img src="{{$product->image}}" alt=""></div>
                <div class="product-button">
                  <a class="add_cart" data-slug="{{$product->slug}}" ><i class="add_icon fa fa-shopping-bag" ></i></a> 
                  <a href="/products/{{$product->slug}}" ><i class="fa fa-search"></i></a> 
                </div>
              </div>
              <div class="product-detail">
                <p class="product-title"><a href="/">{{$product->name}}</a></p>
                <strong class="item-price">{{$product->price}} - weGFT Credits</strong>
              </div>
            </div>
            </div>
            @endforeach
          </div>
          @else
          <div class="text-center">
            <h3>No Products Found</h3>
          </div>
          @endif
        </div>
      </div>
    </div>
  </section>
</div>
@stop