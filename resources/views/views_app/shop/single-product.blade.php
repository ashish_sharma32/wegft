@extends('layout.app')
@section('title') {{$product->name}} | Shop - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link"> <a href="/">Home</a> <a href="/shop">Shop</a> <span>{{ $product->name }}</span> </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page single-product-content">
    <div id="product-detail" class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 mb-30">
          <div class="product-page-image">
            <div class="product-image-slider product-image-gallery" id="product-image-gallery" data-pswp-uid="3">
              <div class="item">
                <a class="product-gallery-item" href="#" data-size="" data-med="{{$product->image}}" data-med-size="">
                <img src="{{$product->image}}">
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 mb-30">
          <div class="product-page-content">
            <h2 class="product-title">{{$product->name}}</h2>
            <div class="product-rating">
              <div class="star-rating" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating" title="Rated 4 out of 5"> <span style="width: 60%"></span></div>
              <a href="#" class="product-rating-count"><span class="count">3</span> Reviews</a>
            </div>
            <div class="product-price"> <span><span class="product-price-sign">$</span><span class="product-price-text">{{$product->price}}</span></span>
            </div>
            <p class="product-description"> Product Description </p>
            <form class="single-variation-wrap">
              <a href="/add-to-cart/{{$product->slug}}" class="add_cart btn btn-lg btn-black" data-slug="{{$product->slug}}"><i class="add_icon fa fa-shopping-bag"></i>Add to cart</a>
            </form>
            <div class="product-meta"> <span>Category : <span class="category" itemprop="category">{{$product->category->name}}</span></span>
            </div>
            <div class="product-share">
              <span>Share :</span>
              <ul>
                <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://nileforest.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="http://twitter.com/share?url=http://nileforest.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="http://plus.google.com/share?url=http://nileforest.com/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="mailto:?subject=Check this http://nileforest.com/" target="_blank"><i class="fa fa-envelope"></i></a></li>
                <li><a href="http://pinterest.com/pin/create/button/?url=http://nileforest.com/exampleImage.jpg" target="_blank"><i class="fa fa-pinterest"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="product-tabs-wrapper container">
      <ul class="product-content-tabs nav nav-tabs" role="tablist">
        <li class="nav-item"> <a class="active" href="#tab_description" role="tab" data-toggle="tab">Description</a></li>
      </ul>
      <div class="product-content-Tabs_wraper tab-content container">
        <div id="tab_description" role="tabpanel" class="tab-pane fade in active">
          <h6 class="product-collapse-title" data-toggle="collapse" data-target="#tab_description-coll">Description</h6>
          <div id="tab_description-coll" class="shop_description product-collapse collapse container">
            <div class="row">
              <div class=" col-md-6">
                <p> Etiam molestie sit amet arcu vel dictum. Integer mattis est nec porta porttitor. Maecenas condimentum sapien eget urna condimentum, non sagittis ante dapibus. Donec congue libero ut ex malesuada auctor. Vivamus at urna et erat aliquam pharetra. Nulla facilisi. Morbi feugiat tortor finibus elit aliquet tempor. Generated 5 paragraphs, 453 words, 3065 bytes of Lorem Ipsum</p>
                <h4>Vivamus at urna</h4>
                <ul>
                  <li>Etiam molestie sit amet arcu vel dictum</li>
                  <li>Integer mattis est nec porta porttitor</li>
                  <li>Maecenas condimentum sapien eget urna condimentum</li>
                  <li>Donec congue libero ut ex malesuada auctor</li>
                  <li>Generated 5 paragraphs, 453 words</li>
                </ul>
              </div>
              <div class="col-md-6">
                <p> Etiam molestie sit amet arcu vel dictum. Integer mattis est nec porta porttitor. Maecenas condimentum sapien eget urna condimentum, non sagittis ante dapibus. Donec congue libero ut ex malesuada auctor. Vivamus at urna et erat aliquam pharetra. Nulla facilisi. Morbi feugiat tortor finibus elit aliquet tempor. Generated 5 paragraphs, 453 words, 3065 bytes of Lorem Ipsum</p>
                <h4>hadding four</h4>
                <h5>hadding five</h5>
                <h6>hadding six</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@stop