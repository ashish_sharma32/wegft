@extends('layout.app')
@section('title') Order Details - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link"> <a href="/">Home</a> <span>Order Details</span> </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="normal"><span>Order Details</span></h2>
          <div class="alert alert-info">
            <p class="mb-25">Order <strong>#{{$order->id}}</strong> was placed on <strong>{{$order->created_at->toDateString()}}</strong></p>
          </div>

          <div class="table-responsive">          
            <table class="table">
              <thead>
                <tr>
                  <th style="width:60%">Product</th>
                  <th style="width:40%">Total</th>
                </tr>
              </thead>
              <tbody>
              @foreach($products as $product)
              <tr>
                <td>{{$product['item']->name}} * {{$product['qty']}}</td>
                <td>{{$product['price']}} weGFT Credits</td>
              </tr>
              @endforeach
              <tr>
                <td><strong>SUBTOTAL</strong></td>
                <td>{{$order->totalAmount}} weGFT Credits</td>
              </tr>
              <tr>
                <td><strong>PAYMENT METHOD</strong></td>
                <td>Coupon Code : <strong>{{$order->coupon_code}}</strong></td>
              </tr>
              <tr>
                <td><strong>TOTAL</strong></td>
                <td>{{$order->totalAmount}} weGFT Credits</td>
              </tr>

              </tbody>
            </table>
          </div>
          <h6>Shipping Address</h6>
          <p class="mb-25"> {{$order->firstname}} {{$order->lastname}}
            <br> {{$order->address_street}},
            <br> {{$order->address_city}}, {{$order->address_state}} 
            <br> {{$order->address_zip}}
          </p>
        </div>
      </div>
    </div>
  </section>
</div>
@stop