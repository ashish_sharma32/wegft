@extends('layout.app')
@section('title') Order Success | Checkout - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link">
            <a href="#">Home</a>
            <span>Checkout</span>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page">
    <div class="container mb-80">
      <div class="row">
        <div class="col-sm-12">
          <article class="post-8">
            {{ Form::open(array('url' => '/orders','class'=>'product-checkout mt-45')) }}
            <div class="row">
              <div class="col-md-12">
                <div class="checkout-order-review">
                  <div class="order-success">
                    <i aria-hidden="true" class="fa fa-check fa-4x"></i>
                  </div>
                  <h3>Your order is successfull</h3>
                  <div class="product-checkout-review-order">
                    <div class="responsive-table">
                      <table class="">
                        <thead>
                          <tr>
                            <th class="product-name">Product</th>
                            <th class="product-total">Total</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($products as $product)
                          <tr class="cart_item">
                            <td class="product-name">{{$product['item']->name}}<strong> x {{$product['qty']}}</strong></td>
                            <td class="product-total">
                              <span class="product-price-amount amount">{{$product['qty'] * $product['item']->price }} weGFT Credits</span>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                        <tfoot>
                          <tr class="order-total">
                            <th>Total</th>
                            <td>
                              <span class="product-price-amount amount">{{$totalPrice}} weGFT Credits</span>
                            </td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                    <div class="product-checkout-payment">
                      <ul>
                      </ul>
                      <div class="place-order">
                        <a class="btn btn-lg btn-color form-half-width" href="/">Back To Shop</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{ Form::close() }}
          </article>
        </div>
      </div>
    </div>
  </section>
</div>
@stop