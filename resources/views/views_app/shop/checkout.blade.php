@extends('layout.app')
@section('title') Checkout - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link">
            <a href="#">Home</a>
            <span>Checkout</span>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page">
    <div class="container mb-80">
      <div class="row">
        <div class="col-sm-12">
          @if(Session::has('coupon'))
            @if(is_null($products))
                <div class="text-center">
                  <h3>Shopping Cart Empty</h3>
                  <div><a class="btn btn-lg btn-color form-half-width" href="/">Back To Shop</a></div>
                </div>
            @else
          <article class="post-8">
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger">{{ $error }}</div>
            @endforeach
            {{ Form::model(Auth::user(),array('url' => '/orders','class'=>'product-checkout mt-45')) }}
            <input type="hidden" name="coupon_code" value="{{Session::get('coupon')->code}}">
            <div class="row">
              <div class="col-md-6">
                <h3>Billing details</h3>
                <div class="row">
                  <div class="form-field-wrapper form-center col-sm-6">
                    <label class="left">
                    First Name
                    <abbr class="form-required" title="required">*</abbr></label>
                    {{Form::text('firstname',null,['class' => 'input-md form-full-width', 'required'=>''])}}
                  </div>
                  <div class="form-field-wrapper form-center col-sm-6">
                    <label class="left">
                    Last Name
                    <abbr class="form-required" title="required">*</abbr></label>
                    {{Form::text('lastname',null,['class' => 'input-md form-full-width', 'required'=>''])}}
                  </div>
                  <div class="form-field-wrapper form-center col-sm-12">
                    <label class="left">
                    Address
                    <abbr class="form-required" title="required">*</abbr></label>
                    {{Form::text('address_street', null, ['class' => 'input-md form-full-width mb-20', 'required' => ''])}}
                  </div>
                  <div class="form-field-wrapper form-center col-sm-12">
                    <label class="left">
                    Town / City
                    <abbr class="form-required" title="required">*</abbr></label>
                    {{Form::text('address_city', null, ['class' => 'input-md form-full-width', 'required'=>''])}}
                  </div>
                  <div class="form-field-wrapper form-center col-sm-12">
                    <label class="left">
                    State
                    <abbr class="form-required" title="required">*</abbr></label>
                    {{Form::text('address_state', null, ['class' => 'input-md form-full-width','required'=>''])}}
                  </div>
                  <div class="form-field-wrapper form-center col-sm-12">
                    <label class="left">
                    Postcode / ZIP
                    <abbr class="form-required" title="required">*</abbr></label>
                    {{Form::text('address_zip', null, ['class' => 'input-md form-full-width', 'required'=>''])}}
                  </div>
                  <div class="form-field-wrapper form-center col-sm-6">
                    <label class="left">
                    Phone
                    <abbr class="form-required" title="required">*</abbr></label>
                    {{Form::tel('phone', null, ['class' => 'input-md form-full-width','required'=>''])}}
                  </div>
                  <div class="form-field-wrapper form-center col-sm-6">
                    <label class="left">
                    Email
                    <abbr class="form-required" title="required">*</abbr></label>
                    {{Form::email('email', null, ['class' => 'input-md form-full-width','required'=>''])}}
                  </div>
                </div>
              </div>
              <div class="col-md-6">
<!--                 <div class="checkout-apply-coupon">
                  <h3>Apply Coupon</h3>
                </div> -->
                <div class="checkout-order-review">
                  <h3>Your order</h3>
                  @if(session()->has('error_coupon'))
                  <div class="alert alert-danger">The Coupon Credits Value is Less than Cart.<br/>Get More Credits Here <a href="https://dankendo.com/subscription/" target="_blank">Dan Kendo</a></div>
                  @endif
                  <div class="product-checkout-review-order">
                    <div class="responsive-table">
                      <table class="">
                        <thead>
                          <tr>
                            <th class="product-name">Product</th>
                            <th class="product-total">Total</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($products as $product)
                          <tr class="cart_item">
                            <td class="product-name">{{$product['item']->name}}<strong> x {{$product['qty']}}</strong></td>
                            <td class="product-total">
                              <span class="product-price-amount amount">{{$product['qty'] * $product['item']->price }} weGFT Credits</span>
                            </td>
                          </tr>
                          @endforeach
                          <tr class="cart_item order-credits">
                            <td class="product-name"> - (weGFT Credits Applied) </td>
                            <td class="product-total">
                              <span class="product-price-amount amount wegft-credits-amount"></span>
                            </td>
                          </tr>                        
                        </tbody>
                        <tfoot>
                          <tr class="order-total">
                            <th>Total</th>
                            <td>
                              <span class="initial-billing-amount">{{ $totalPrice }}</span>
                              @if(Session::has('coupon'))
                              <span class="product-price-amount amount billing-total">{{ $totalPrice - Session::get('coupon')->amount }}</span><span> weGFT Credits</span>
                              @else
                              <span class="product-price-amount amount billing-total">{{ $totalPrice }}</span><span> weGFT Credits</span>
                              @endif
                            </td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                    <div class="product-checkout-payment">
                      <ul>
                      </ul>
                      <div class="place-order">
                        <button class="btn btn-lg btn-color form-full-width" type="submit">Place Order</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{ Form::close() }}
          </article>
          @endif
          @else
          <div class="text-center">
            <h3>Coupon Not Applied Yet</h3>
            <p>Go Back To Cart and Apply Coupon</p>
            <div><a class="btn btn-lg btn-color form-half-width" href="/cart">Back To Cart</a></div>
          </div>
          @endif
        </div>
      </div>
    </div>
  </section>
</div>
@stop