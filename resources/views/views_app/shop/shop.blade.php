@extends('layout.app')
@section('title') Shop - weGFT @stop
@section('page-content')
<div class="wraper">
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link"> <a href="/">Home</a> <span>Shop</span> </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="list-page-title">
            <h2 class="">Shop</h2>
          </div>

          <div class="row product-list-item">
            @foreach ($products as $product)
            <div class="product-item-element col-sm-6 col-md-4 col-lg-3 wow fadeIn" data-wow-delay="0.1s">
              <div class="product-item">
              <div class="product-item-inner">
                <div class="product-img-wrap"> <img src="{{$product->image}}" alt=""></div>
                <div class="product-button">
                  <a class="add_cart" data-slug="{{$product->slug}}" ><i class="add_icon fa fa-shopping-bag" ></i></a> 
                  <a href="/products/{{$product->slug}}" ><i class="fa fa-search"></i></a> 
                </div>
              </div>
              <div class="product-detail">
                <p class="product-title"><a href="/">{{$product->name}}</a></p>
                <strong class="item-price">{{$product->price}} - weGFT Credits</strong>
              </div>
            </div>
            </div>
            @endforeach
          </div>
          <div class="row">
            <div class="col-md-12 text-center">
              <br/>
              <button class="btn btn-md btn-color load-products" data-id="{{$product->id}}">Load More</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@stop
@section('static-scripts')
<script type="text/javascript">
  new WOW().init();
  $spinner = "<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>";
  $(".load-products").click(function(event) {
    $lastProductId = $(this).data('id');
    $loadBtn = $(".load-products");
    $loadBtn.html($spinner);
    $.ajax({
      type: "post",
      url: "/load-products",
      data: {
        'lastProductId': $lastProductId,
        '_token': '{{csrf_token()}}',
      },
      success: function(data){
        if (!$.trim(data)) {
          $loadBtn.text("No More Products");
          $loadBtn.removeAttr('data-id');
        }else {
          $loadBtn.data("id", data.id);
          $container = $(".product-list-item");
          $container.append(data.products);
          $loadBtn.html("Load More");  
        }
      }
    });
  });
</script>
@stop