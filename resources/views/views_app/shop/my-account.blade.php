@extends('layout.app')
@section('title') My Account - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link"> <a href="#">Home</a> <span>My Account</span> </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page single-product-content">
    <div class="product-tabs-wrapper container ">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#orders" role="tab" data-toggle="tab">Orders</a>
        </li>
      </ul>
      <div class="tab-content col-md-12">
        <div role="tabpanel" class="tab-pane fade in active" id="profile">
          <h6 class="text-center">Account Details</h6>
          <div class="row">
            <div class="col-md-12 text-center avatar">
              <img src="{{Auth::user()->avatar}}" alt="avatar" />
            </div>
            <div class="col-md-12">
              {{Form::model(Auth::user())}}
              <fieldset disabled="">
                <div class="form-field-wrapper">
                  <label>First Name</label>
                  {{Form::text('firstname',null,['class' => 'input-md form-full-width'])}}
                </div>
                <div class="form-field-wrapper">
                  <label>Last Name</label>
                  {{Form::text('lastname',null,['class' => 'input-md form-full-width'])}}
                </div>
                <div class="form-field-wrapper">
                  <label>Email Address</label>
                  {{Form::email('email', null, ['class' => 'input-md form-full-width'])}}
                </div>
                <div class="form-field-wrapper">
                  <label>Phone</label>
                  {{Form::tel('phone', null, ['class' => 'input-md form-full-width'])}}
                </div>
                <div class="row">
                  <div class="form-field-wrapper col-md-6">
                    <label>Address Street</label>
                    {{Form::text('address_street', null, ['class' => 'input-md form-full-width'])}}
                  </div>
                  <div class="form-field-wrapper col-md-6">
                    <label>Address City</label>
                    {{Form::text('address_city', null, ['class' => 'input-md form-full-width'])}}
                  </div>
                </div>
                <div class="row">
                  <div class="form-field-wrapper col-md-6">
                    <label>Address State</label>
                    {{Form::text('address_state', null, ['class' => 'input-md form-full-width'])}}
                  </div>
                  <div class="form-field-wrapper col-md-6">
                    <label>Address Zip</label>
                    {{Form::text('address_zip', null, ['class' => 'input-md form-full-width'])}}
                  </div>
                </div>
              </fieldset>
              {{Form::close()}}
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="orders">
          @if(!$orders->isEmpty())
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Ship To</th>
                <th>Order Date</th>
                <th>Customer Name</th>
                <th>Order Status</th>
              </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)
              <tr>
                <td><a href="/order-details/{{$order->id}}">{{$order->id}}</a></td>
                <td>{{$order->address_street}}, {{$order->address_city}}<br/>{{$order->address_state}},{{$order->address_zip}}</td>
                <td></td>
                <td>{{$order->firstname}} {{$order->lastname}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <h3 style="text-align: center">No Orders</h3>
          @endif
        </div>
      </div>
    </div>
  </section>
</div>
@stop