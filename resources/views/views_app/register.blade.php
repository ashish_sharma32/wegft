@extends('layout.app')
@section('title') Create A New Account - weGFT @stop
@section('page-content')
<div class="page-content-wraper">
  <section class="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="breadcrumb-link"> <a href="#">Home</a> <span>Register</span> </nav>
        </div>
      </div>
    </div>
  </section>
  <section class="content-page">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="form-border-box">
            {!! Form::open(array('url' => '/users', 'id' => 'user_register')) !!}
            <h2 class="normal"><span>Create A New Account</span></h2>
            <div class="form-field-wrapper">
              <label>Enter First Name <span class="required">*</span></label>
              <input class="input-md form-full-width" name="firstname" aria-required="true" required type="text">
            </div>
            <div class="form-field-wrapper">
              <label>Enter Last Name <span class="required">*</span></label>
              <input class="input-md form-full-width" name="lastname" aria-required="true" required type="text">
            </div>
            <div class="form-field-wrapper">
              <label>Enter E-mail Address <span class="required">*</span></label>
              <input class="input-md form-full-width" name="email" aria-required="true" required type="email">
            </div>
            <div class="form-field-wrapper">
              <label>Enter Your Password <span class="required">*</span></label>
              <input class="input-md form-full-width" name="password" aria-required="true" required type="password">
            </div>
            <div class="form-field-wrapper">
              <input class="submit btn btn-md btn-black" type="submit">
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@stop