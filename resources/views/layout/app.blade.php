<!DOCTYPE html>
<html>
<head>
    @include('partials.app.meta')
    @include('partials.app.styles')
</head>
<body>
    <section id="sidebar-right" class="sidebar-menu sidebar-right">
        <div class="cart-sidebar-wrap">
            <div class="cart-widget-heading">
                <h4>Shopping Cart</h4>
                <a href="javascript:void(0)" id="sidebar_close_icon" class="close-icon-white"></a>
            </div>
            <div class="cart-widget-content">
                <div class="cart-widget-product ">
                    <div class="cart-empty">
                        <p>You have no items in your shopping cart.</p>
                    </div>
                    <ul class="cart-product-item">
                    </ul>
                </div>
            </div>
            <div class="cart-widget-footer">
                <div class="cart-footer-inner">
                    <h4 class="cart-total-hedding normal"><span>Total :</span><span class="cart-total-price"></span></h4>
                    <div class="cart-action-buttons"> <a href="/cart" class="view-cart btn btn-md btn-gray">View Cart</a> <a href="/checkout" class="checkout btn btn-md btn-color">Checkout</a></div>
                </div>
            </div>
        </div>
    </section>
    <div class="sidebar_overlay"></div>
    <section class="search-overlay-menu">
        <a href="javascript:void(0)" class="search-overlay-close"></a>
        <div class="container">
            <form role="search" id="search">
                <div class="search-icon-lg">
                    {{ HTML::image('img/search-icon-lg.png', 'search') }}
                </div>
                <label class="h6 normal search-input-label" for="search-query">Enter keywords to Search Product</label>
                <input value="" name="q" type="search" placeholder="Search...">
                <button type="submit">
                    {{ HTML::image('img/search-lg-go-icon.png') }}
                </button>
            </form>
        </div>
    </section>
    <div class="wraper">
        <header class="header">
            <div class="header-topbar">
                <div class="header-topbar-inner">
                    <div class="topbar-right">
                        <ul class="list-none">
                            <li class="dropdown-nav">
                                <a href="#"><i class="fa fa-user left" aria-hidden="true"></i><span class="hidden-sm-down">My Account</span><i class="fa fa-angle-down right" aria-hidden="true"></i></a>
                                <div class="dropdown-menu">
                                    <ul>
                                        <li><a href="/my-account">My Account</a></li>
                                        <li><a href="/my-account#tab_orders">Order History</a></li>
                                    </ul>
                                    @if (Auth::check())
                                    <span class="divider"></span>
                                    <ul>
                                        <li><a href="/logout"><i class="fa fa-lock left" aria-hidden="true"></i>Logout</a></li>
                                    </ul>
                                    @else
                                    <span class="divider"></span>
                                    <ul>
                                        <li><a href="/login"><i class="fa fa-lock left" aria-hidden="true"></i>Login</a></li>
                                        <li><a href="/register"><i class="fa fa-user left" aria-hidden="true"></i>Create an Account</a></li>
                                    </ul>
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="header-sticky" class="header-main">
                <div class="header-main-inner">
                    <div class="logo">
                        <a href="/">{{ HTML::image('img/logo_test.png', 'weGFT') }}</a>
                    </div>
                    <div class="header-rightside-nav">
                        <div class="sidebar-icon-nav">
                            <ul class="list-none-ib">
                                <li><a id="search-overlay-menu-btn"><i aria-hidden="true" class="fa fa-search"></i></a></li>
                                <li>
                                    <a id="sidebar_toggle_btn">
                                        <div class="cart-icon"> <i aria-hidden="true" class="fa fa-shopping-bag"></i></div>
                                        
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <nav class="navigation-menu">
                        <ul>
                            <li> <a href="/">Home</a></li>
                            <li> <a href="/about">About Us</a></li>
                            <li> <a href="/shop">Shop</a></li>
                            <li> <a href="/contact">Contact Us</a></li>

                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        @yield('page-content')
        <footer class="footer section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 mb-sm-45">
                        <div class="footer-block about-us-block">
                            {{ HTML::image('img/logo_white.png', 'weGFT', array('width' => '125')) }}
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <ul class="footer-social-icon list-none-ib">
                                <li><a href="http://facebook.com/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 mb-sm-45">
                        <div class="footer-block information-block">
                            <h6>Quick Links</h6>
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="/about">About</a></li>
                                <li><a href="/shop">Shop</a></li>
                                <li><a href="/">Privacy Policy</a></li>
                                <li><a href="/">Terms &amp; Condition</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <div class="footer-block contact-block">
                            <h6>Contact</h6>
                            <ul>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>
                                Street Address
                                </li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@wegft.com">info@wegft.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="footer-newsletter">
                </div>
            </div>
            <div class="container">
                <div class="copyrights">
                    <p class="copyright">&copy; weGFT - 2017 | All Rights Reserved</p>
                </div>
            </div>
        </footer>
    </div>
    @include('partials.app.scripts')
    @yield('static-scripts')
</body>
</html>