<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials.admin.meta')
    @include('partials.admin.styles')
  </head>
  <body>
    <div class="be-wrapper">
      <nav class="navbar navbar-default navbar-fixed-top be-top-header">
        <div class="container-fluid">
          <div class="navbar-header"><a href="" class="navbar-brand"></a></div>
          <div class="be-right-navbar">
            <ul class="nav navbar-nav navbar-right be-user-nav">
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="/admin_assets/img/avatar.png" alt="Avatar"><span class="user-name">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span></a>
                <ul role="menu" class="dropdown-menu">
                  <li>
                    <div class="user-info">
                      <div class="user-name">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</div>
                    </div>
                  </li>
                  <li><a href="#"><span class="icon mdi mdi-face"></span>Account</a></li>
                  <li><a href="#"><span class="icon mdi mdi-settings"></span>Settings</a></li>
                  <li><a href="/logout"><span class="icon mdi mdi-power"></span>Logout</a></li>
                </ul>
              </li>
            </ul>
            <div class="page-title"><span>weGFT Admin Panel</span></div>
          </div>
        </div>
      </nav>
      <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Menu</a>
          <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
              <div class="left-sidebar-content">
                <ul class="sidebar-elements">
                  <li><a href="/admin"><span>Dashboard</span></a></li>
                  <li><a href="/admin/products"><span>Products Management</span></a></li>
                  <li><a href="/admin/categories"><span>Categories Management</span></a></li>
                  <li><a href="/admin/orders"><span>Orders Management</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="be-content">
        <div class="page-head">
          <h2 class="page-head-title">Dashboard</h2>
          <ol class="breadcrumb page-head-nav">
            <li><a href="/admin">Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </div>
        <div class="main-content container-fluid">
          @yield('page-content')
        </div>
      </div>
    </div>
    @include('partials.admin.scripts')
  </body>
</html>