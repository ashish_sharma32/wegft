<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('products')->insert([
            [
            'name' => 'Vape Cart 3 Pack',
            'category_id' => '1',
            'slug' => 'vape-cart-3-pack',
            'price' => '75',
            'image' => '/uploads/products/img1.jpg'
            ],
            [
            'name' => 'Krispy Treat',
            'category_id' => '2',
            'slug' => 'krispy-treat',
            'price' => '75',
            'image' => '/uploads/products/img2.jpg'
            ],
            [
            'name' => 'THC Caps',
            'category_id' => '3',
            'slug' => 'thc-caps',
            'price' => '75',
            'image' => '/uploads/products/img3.jpg'
            ],
            [
            'name' => 'Sugar Sauce',
            'category_id' => '1',
            'slug' => 'sugar-sauce',
            'price' => '75',
            'image' => '/uploads/products/img4.jpg'
            ],
            [
            'name' => 'Sugar Sauce 3 Pack',
            'category_id' => '2',
            'slug' => 'sugar-sauce-3-pack',
            'price' => '75',
            'image' => '/uploads/products/img5.jpg'
            ],
            [
            'name' => 'Pre Rolls 5 Pack',
            'category_id' => '3',
            'slug' => 'pre-rolls-5-pack',
            'price' => '75',
            'image' => '/uploads/products/img6.jpg'
            ],
        ]);
        
        for($i = 0; $i < 100; $i++) {
            $name = $faker->name;
            $slug = str_slug($name);
            App\Product::create([
                'name' => $name,
                'category_id' => $faker->numberBetween(1,3),
                'slug' => $slug,
                'price' => $faker->numberBetween(1,75),
                'image' => '/uploads/products/img1.jpg'
            ]);
        }
    }
}
