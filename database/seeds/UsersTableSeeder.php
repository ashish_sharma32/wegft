<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'firstname' => 'Ashish',
            'lastname' => 'Sharma',
            'email' => 'sharma.asmith7@gmail.com',
            'password' => bcrypt('ASHISH1232'),
            'admin' => '1',
            ]
        ]);
    }
}
