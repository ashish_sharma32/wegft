<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','SiteController@home');
Route::get('/about','SiteController@about');
Route::get('/shop','SiteController@shop');
Route::get('/shop/product','SiteController@singleProduct');
Route::get('/contact','SiteController@contact');
Route::get('/search/{query}','ProductController@search');
Route::get('/get-billing','CouponController@getBillingFromCoupon');
Route::get('auth/{provider}', 'SocialAuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'SocialAuthController@handleProviderCallback');
Route::get('add-to-cart/{slug}','ProductController@addToCart');
Route::get('remove-from-cart/{slug}','ProductController@removeFromCart');
Route::get('get-cart','ProductController@getCart');
Route::get('/has-coupon','CouponController@hasCoupon');
Route::get('/remove-code','CouponController@removeCoupon');
Route::post('/load-products','ProductController@loadProducts');

/*Resource*/
Route::resource('users','UserController');
Route::resource('orders','OrderController');
Route::resource('products','ProductController');
Route::resource('categories','CategoriesController');

/*Auth Only*/
Route::group(['middleware' => ['isAuth']], function () {
	Route::get('/my-account','SiteController@account');
	Route::get('/order-details/{id}','OrderController@show');
	Route::get('/cart','SiteController@cart');
	Route::get('/checkout','SiteController@checkout');
	Route::post('/redeem/','CouponController@redeem');
	Route::get('/logout','UserController@logout');
	Route::get('/redeem-code/{code}','CouponController@redeemFromRefer');
	Route::resource('coupons','CouponController');
});

/*Guest Only*/
Route::group(['middleware' => ['isGuest']], function () {
	Route::get('/login','SiteController@login');
	Route::get('/admin/login','AdminController@login');
	Route::post('/login','UserController@login');
	Route::get('/register','SiteController@register');
});

/*Admin Only*/
Route::group(['middleware' => ['isAdmin'] , 'prefix' => 'admin'], function () {
	Route::get('/','AdminController@dashboard');
	/*Products*/
	Route::get('/products','AdminController@products');
	Route::get('/products/create','AdminController@createProduct');
	Route::get('/products/{product}/edit','AdminController@editProduct');
	Route::post('/products/store','AdminController@storeProduct');
	Route::put('/products/{product}','AdminController@updateProduct');
	Route::delete('/products/{product}','AdminController@deleteProduct');
	
	/*Categories*/
	Route::get('/categories','AdminController@categories');
	Route::get('/categories/create','AdminController@createCategory');
	Route::post('/categories/store','AdminController@storeCategory');
	Route::get('/categories/{category}/edit','AdminController@editCategory');
	Route::put('/categories/{category}','AdminController@updateCategory');
	Route::delete('/categories/{category}','AdminController@deleteCategory');
	
	/*Orders*/
	Route::get('/orders','AdminController@orders');
	Route::get('/orders/{order}','AdminController@showOrder');
	Route::delete('/orders/{order}','AdminController@deleteOrder');
});